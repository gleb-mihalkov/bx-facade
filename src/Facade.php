<?php
namespace Bx\Facade;
use Bx\Singleton\Singleton;
use Bx\Assert\Assert;

/**
 * Реализация статического фасада.
 */
trait Facade
{
    use Singleton;

    /**
     * Возвращает новое имя метода перед его вызовом. По умолчанию имя метода остается
     * без изменений.
     * @param  string $method Имя метода.
     * @return string         Новое имя метода.
     */
    protected static function convertMethodName(string $method) : string
    {
        return $method;
    }

    /**
     * Обрабатывает вызов недоступного метода класса.
     * @param  string  $method Имя метода.
     * @param  array   $args   Массив агрументов метода.
     * @return mixed           Результат работы метода.
     */
    public static function __callStatic($method, $args)
    {
        $instance = self::getInstance();

        $method = static::convertMethodName($method);
        Assert::notEmpty($method);
        Assert::methodExists($instance, $method);

        $result = call_user_func_array([$instance, $method], $args);
        return $result;
    }
}
